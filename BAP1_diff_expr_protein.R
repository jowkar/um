library(readxl)
library(stringr)

outdir <- "~/um/Figures_and_tables/Supplement/"
dir.create(outdir,recursive = T,showWarnings = F)
pth <- "~/um/code/data/Figure_S5fgh/3278_SPhuman_QMS3_Lumos_191014_FAIMS_71-90_5%FDRproteins.xlsx"

sheets <- excel_sheets(pth)
x <- list()
for (sheet in sheets){
  x[[sheet]] <- read_excel(pth,sheet = sheet)
}
x <- x$Proteins

cnames <- colnames(x)
x <- x[2:nrow(x),]
x <- x[,c("...3","ctl1","ctl2","ctl3","BAP1_1","BAP1_2","BAP1_3")]
x <- as.data.frame(x,stringsAsFactors=F)
rownames(x) <- x$...3
x$...3 <- NULL

x$ctl1 <- as.numeric(x$ctl1)
x$ctl2 <- as.numeric(x$ctl2)
x$ctl3 <- as.numeric(x$ctl3)
x$BAP1_1 <- as.numeric(x$BAP1_1)
x$BAP1_2 <- as.numeric(x$BAP1_2)
x$BAP1_3 <- as.numeric(x$BAP1_3)
stopifnot(min(x)>0)
x.log2 <- log2(x)

conditions <- c(rep("ctl",3),rep("BAP1",3))
deps_group <- factor(conditions)

analysis_limma <- function(expr_data_frame, group, comparison_factor) {
  requireNamespace("limma")
  requireNamespace("stats")
  
  design <- stats::model.matrix(~0 + group)
  colnames(design) <- levels(factor(group))
  rownames(design) <- colnames(expr_data_frame)
  group_levels <- comparison_factor
  
  comparison_statement <- paste0(comparison_factor[2],"-",comparison_factor[1])
  
  contrast.matrix <- limma::makeContrasts(contrasts = comparison_statement, 
                                          levels = design)
  
  fit <- limma::lmFit(expr_data_frame, design)
  fit2 <- limma::contrasts.fit(fit, contrast.matrix)
  fit2 <- limma::eBayes(fit2)
  alls <- limma::topTable(fit2, coef = comparison_statement, adjust.method = "none", 
                          p.value = 1, number = Inf)
  
  results_df <- data.frame(ID=rownames(alls), logFC=alls$logFC, pvalue=alls$P.Value, q=p.adjust(alls$P.Value,method="BH"))
  
  results_df <- results_df[order(results_df$pvalue),]
  results_df$protein_name <- str_split_fixed(results_df$ID,"_",4)[,4]
  
  return(results_df)
}

limma_results_df_BAP1_over_ctl <- analysis_limma(x.log2,deps_group,c("ctl","BAP1"))
limma_results_df_BAP1_over_ctl$protein_name <- str_split_fixed(str_split_fixed(limma_results_df_BAP1_over_ctl$ID,"GN=",2)[,2]," PE=",2)[,1]
limma_results_df_BAP1_over_ctl <- limma_results_df_BAP1_over_ctl[which(limma_results_df_BAP1_over_ctl$protein_name!=""),]

rna_de <- read_excel("~/um/Figures_and_tables/Supplement/Supplementary_Data_4.xlsx",
                     col_types = c("text","text","numeric","numeric","numeric","numeric","numeric"))
rna_de <- as.data.frame(rna_de,stringsAsFactors=F)

get_alias <- function(limma_results_df_BAP1_over_ctl,rna_de){
  library(biomaRt)
  ensembl= useMart(biomart="ENSEMBL_MART_ENSEMBL", host="grch37.ensembl.org", path="/biomart/martservice",
                   dataset="hsapiens_gene_ensembl") # last hg19 release
  attributes <- c("external_gene_name","ensembl_gene_id")
  map_hg19 <- getBM(attributes=attributes,mart=ensembl)
  ensembl_hg38= useMart(biomart="ensembl",dataset="hsapiens_gene_ensembl")
  map_hg38 <- getBM(attributes=attributes,mart=ensembl_hg38)
  map <- rbind(map_hg19,map_hg38)
  map <- unique(map)
  limma_results_df_BAP1_over_ctl$protein_name_orig <- limma_results_df_BAP1_over_ctl$protein_name
  for (i in 1:nrow(limma_results_df_BAP1_over_ctl)){
    if (! limma_results_df_BAP1_over_ctl$protein_name[i] %in% rna_de$`HUGO gene symbol`){
      ens <- map[which(map$external_gene_name == limma_results_df_BAP1_over_ctl$protein_name[i]),]$ensembl_gene_id
      ens <- ens[!is.na(ens)]
      if (length(ens)>0){
        if (length(intersect(ens,rna_de$`Ensembl gene ID`))>0){
          ens <- intersect(ens,rna_de$`Ensembl gene ID`)
          limma_results_df_BAP1_over_ctl$protein_name[i] <- rna_de$`HUGO gene symbol`[rna_de$`Ensembl gene ID` == ens]
        } else if (length(intersect(ens,map_hg19$ensembl_gene_id))>0){
          limma_results_df_BAP1_over_ctl$protein_name[i] <- unique(as.character(
            map_hg19[which(map_hg19$ensembl_gene_id %in% ens),]$external_gene_name))
        }
      }
    }
  }
  limma_results_df_BAP1_over_ctl$alias <- limma_results_df_BAP1_over_ctl$protein_name
  limma_results_df_BAP1_over_ctl$alias[limma_results_df_BAP1_over_ctl$alias %in%
                                         limma_results_df_BAP1_over_ctl$protein_name_orig] <- ""
  return(limma_results_df_BAP1_over_ctl)
}

limma_results_df_BAP1_over_ctl <- get_alias(limma_results_df_BAP1_over_ctl,rna_de = rna_de)

plot_venn <- function(limma_results_df_BAP1_over_ctl,rna_de){
  isect <- intersect(limma_results_df_BAP1_over_ctl$protein_name,rna_de$`HUGO gene symbol`)
  rna_de.isect <- rna_de[which(rna_de$`HUGO gene symbol` %in% isect),]
  limma_results_df_BAP1_over_ctl.isect <- limma_results_df_BAP1_over_ctl[
    which(limma_results_df_BAP1_over_ctl$protein_name %in% isect),]
    
  rna_de.isect.sig <- rna_de.isect[which(rna_de.isect$q < 0.05),]
  limma_results_df_BAP1_over_ctl.isect.sig <- limma_results_df_BAP1_over_ctl.isect[
    which(limma_results_df_BAP1_over_ctl.isect$q < 0.05),]
  
  rna_de.isect.sig$gene_sign <- paste0(rna_de.isect.sig$`HUGO gene symbol`,"_",sign(rna_de.isect.sig$`log2 fold change`))
  limma_results_df_BAP1_over_ctl.isect.sig$gene_sign <- paste0(limma_results_df_BAP1_over_ctl.isect.sig$protein_name,
                                                               "_",sign(limma_results_df_BAP1_over_ctl.isect.sig$logFC))
  
  library(VennDiagram)
  library(scales)
  venn.diagram(
    x = list(rna=rna_de.isect.sig$gene_sign, protein=limma_results_df_BAP1_over_ctl.isect.sig$gene_sign),
    category.names = c("rna" , "protein"),
    filename = paste0(outdir,"/Figure_S5f.svg"),
    imagetype = 'svg',
    width=6,
    height = 6,
    cex=1,
    fontfamily=1,
    cat.cex=1,
    col=c("#440154ff", '#21908dff'),
    fill = c(alpha("#440154ff",0.3), alpha('#21908dff',0.3)),
    output=F
  )
}

# [Figure S5f]
plot_venn(limma_results_df_BAP1_over_ctl,rna_de)

# [Supplementary Data 6]
library(WriteXLS)
WriteXLS(limma_results_df_BAP1_over_ctl,ExcelFileName = "~/um/Figures_and_tables/Supplement/Supplementary_Data_6.xlsx",
         AutoFilter = T,AdjWidth = T,SheetNames = "Fold changes")

# [Figure S5h] ----------------------------------------------------------------------------------------
library(ggplot2)
library(reshape2)

genes <- c("HMGB1","PTDSS1","PVR","NECTIN2","PVRL2")

protein_names <- str_split_fixed(str_split_fixed(rownames(x),"GN=",2)[,2]," PE=",2)[,1] # Non-matched protein/gene names
x.plot <- x[which(protein_names %in% genes),]
rownames(x.plot) <- str_split_fixed(str_split_fixed(rownames(x.plot),"GN=",2)[,2]," PE=",2)[,1]
x.plot$protein_name <- rownames(x.plot)
x.plot <- reshape2::melt(x.plot)
x.plot$variable <- as.character(x.plot$variable)
x.plot$variable <- substr(x.plot$variable,1,3)

ggplot(x.plot,aes(x=variable,y=value)) + geom_boxplot() + geom_point() + theme_classic() + ylab("Normalized abundace") + xlab(NULL) + 
  facet_wrap(~ protein_name,nrow = 2,scales = "free")
ggsave(file=~"~/um/Figures_and_tables/Supplement/Figure_S5h.pdf",width=6,height=1.5)

# [Figure S5g] ----------------------------------------------------------------------------------------
library(stringr)
library(data.table)
library(fgsea)
library(ggplot2)
library(SeqGSEA)

res <- limma_results_df_BAP1_over_ctl
res <- res[which(res$protein_name!=""),]

gene.set <- list()
gene.set[["ONKEN_UP"]] <- loadGenesets("~/um/code/data/Figure_S5fgh/ONKEN_UVEAL_MELANOMA_UP.gmt", 
                                       unique(res$protein_name), geneID.type="gene.symbol",genesetsize.min = 0, 
                                       genesetsize.max = 10000)
gene.set[["ONKEN_DN"]] <- loadGenesets("~/um/code/data/Figure_S5fgh/ONKEN_UVEAL_MELANOMA_DN.gmt", 
                                       unique(res$protein_name), geneID.type="gene.symbol",genesetsize.min = 0, 
                                       genesetsize.max = 10000)
pathways <- list()
for (pw in names(gene.set)){
  for (i in 1:length(gene.set[[pw]]@GS)){
    gene_set_name <- gene.set[[pw]]@GSNames[[i]]
    gene_set_genes <- gene.set[[pw]]@geneList[gene.set[[pw]]@GS[[i]]]
    pathways[[gene_set_name]] <- gene_set_genes
  }
}

res_order_beta <- res[order(res$logFC,decreasing = T),]
stopifnot(!any(is.na(res_order_beta$q)))

duplicate_genes <- names(which(table(res_order_beta$protein_name)>1))
rows_keep <- list()
for (duplicate_gene in duplicate_genes){
  tmp <- res_order_beta[which(res_order_beta$protein_name == duplicate_gene),]
  rows_keep[[duplicate_gene]] <- tmp[which.max(tmp$logFC),]
}
rows_keep <- do.call("rbind",rows_keep)
res_order_beta <- res_order_beta[! res_order_beta$protein_name %in% duplicate_genes,]
res_order_beta <- rbind(res_order_beta,rows_keep)
res_order_beta <- res_order_beta[order(res_order_beta$logFC,decreasing = T),]

ranks <- res_order_beta$logFC
names(ranks) <- res_order_beta$protein_name

fgseaRes <- fgsea(pathways = pathways, stats = ranks, minSize=0, maxSize=10000, nperm=1000000,nproc=4)

pdf(file="~/um/Figures_and_tables/Supplement/Figure_S5g_down.pdf",width = 4,height = 3)
pthw <- "ONKEN_UVEAL_MELANOMA_DN"
plotEnrichment(pathways[[pthw]],ranks) + labs(title=pthw)
dev.off()

pdf(file="~/um/Figures_and_tables/Supplement/Figure_S5g_up.pdf",width = 4,height = 3)
pthw <- "ONKEN_UVEAL_MELANOMA_UP"
plotEnrichment(pathways[[pthw]],ranks) + labs(title=pthw)
dev.off()