get_cna_gene <- function(seg,genes,map){
  library(biomaRt)
  library(data.table)

  options(warn=2)
  segment_copy <- c()
  missing_genes <- c()
  for (i in 1:length(genes)){
    filterlist=genes[i]
    results <- map[map$external_gene_name==genes[i],][1,]
    if (dim(results)[1]>=1){
      idx_chr <- results$chromosome_name %in% unique(seg$Chr)
      if (any(idx_chr)){
        results <- results[idx_chr,]
        idx <- seg$Chr==results$chromosome_name & seg$Start<=results$end_position & seg$End>results$start_position
        if (any(which(idx))){
          segment_copy[i] <- seg[idx,]$Copy[which.max(abs(seg[idx,]$Copy))]
        } else {
          segment_copy[i] <- NA
        }
      } else {
        segment_copy[i] <- NA
        next
      }
    } else {
      segment_copy[i] <- NA
    }
  }
  options(warn=1)
  
  return(segment_copy)
}
