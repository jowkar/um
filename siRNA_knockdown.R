outdir <- "~/um/Figures_and_tables/Figure_2h_S3c/"

# [Figure 2h, Supplementary Figure S3c] ----------------------------------------------------------
library(ggplot2)

get_rs <- function(res,ids){
  rs <- res[which(names(res) %in% ids)]
  for (id in names(rs)){
    idx_keep <- apply(rs[[id]],2,function(x){!(all(is.na(x)))})
    rs[[id]] <- rs[[id]][,idx_keep]
  }
  for (id in ids){
    rs[[id]]$id <- id
    rs[[id]]$gene <- rownames(rs[[id]])
    rs[[id]] <- reshape2::melt(rs[[id]])
    rs[[id]]$norm <- rs[[id]]$value/mean(rs[[id]]$value[rs[[id]]$gene=="control siRNA"])
  }
  rs <- as.data.frame(do.call("rbind",rs),stringsAsFactors=F)
  return(rs)
}

make_merged_plot <- function(res,ids,genes_main){
  rs <- get_rs(res,ids)
  mn <- list()
  for (gene in unique(rs$gene)){
    mn[[gene]] <- mean(rs$norm[rs$gene==gene])
  }
  mn <- unlist(mn)
  ord <- names(mn)[order(mn,decreasing = T)]
  rs$ord <- NA
  for (i in 1:nrow(rs)){
    rs$ord[i] <- which(ord==rs$gene[i])
  }
  rs$is_main <- rs$gene %in% genes_main
  rs$id <- as.character(rs$id)
  rs$id[grep(rs$id,pattern="92-1")] <- "92-1"
  rs$id[grep(rs$id,pattern="MP41")] <- "MP41"
  rs$id[grep(rs$id,pattern="MP-41")] <- "MP41"
  rs$id[grep(rs$id,pattern="UM22")] <- "UM22"
  rs$id <- factor(rs$id,levels=c("UM22","92-1","MP41"))
  
  g <- ggplot(rs, aes(reorder(gene,ord), norm*100,fill=id)) +
    stat_summary(geom = "bar", fun.y = mean, position = "dodge") +
    stat_summary(geom = "errorbar", fun.data = mean_se, position = "dodge") + 
    geom_hline(yintercept=100,linetype="dashed",color = "gray") +
    geom_point(position=position_jitterdodge(jitter.width=0),size=1) +
    theme_classic() + theme(axis.text.x = element_text(angle = 90, hjust = 1,size=5),legend.title = element_blank()) + 
    xlab(NULL) + ylab("Cell count relative to control (%)") + theme(legend.position=c(0.85,0),
                                                                    legend.text=element_text(size=5)) +
    theme(axis.text=element_text(size=5),axis.title=element_text(size=5)) +
    facet_wrap(~is_main,scales="free_x",drop=TRUE) + theme(strip.background = element_blank(),
                                                           strip.text.x = element_blank())
  print(g)
  return(list(ord=unique(data.frame(gene=rs$gene,ord=rs$ord)),g=g))
}

run_test <- function(res,ids){
  rs <- get_rs(res,ids)
  library(lmPerm)
  aov_out_gene <- list()
  cfint <- list()
  coefs <- list()
  for (gene in setdiff(unique(rs$gene),"control siRNA")){
    res.aovp <- aovp(norm ~ id + gene, data = rs[rs$gene %in% c(gene,"control siRNA"),],perm = "Prob",maxIter=10^7,
                     Ca=10^-7)
    cfint[[gene]] <- as.data.frame(confint(res.aovp,level=0.95),stringsAsFactors = F)
    cfint[[gene]]$gene <- gene
    cfint[[gene]]$var <- gsub(rownames(cfint[[gene]]),pattern="[\\(\\)]",replacement="")
    
    coefs[[gene]] <- as.data.frame(coefficients(res.aovp),stringsAsFactors=F)
    colnames(coefs[[gene]]) <- "coefficient"
    coefs[[gene]]$gene <- gene
    coefs[[gene]]$var <- gsub(rownames(coefs[[gene]]),pattern="[\\(\\)]",replacement="")
    
    res.aovp <- summary(res.aovp)
    colnames(res.aovp[[1]])
    out <- data.frame(var=rownames(res.aovp[[1]]),
                      p=res.aovp[[1]][["Pr(Prob)"]],
                      Df=res.aovp[[1]][["Df"]],
                      R_sum_Sq=res.aovp[[1]][["R Sum Sq"]],
                      R_Mean_Sq=res.aovp[[1]][["R Mean Sq"]],
                      stringsAsFactors = F)
    out$gene <- gene
    out$var <- gsub(out$var,pattern = "[ ]+",replacement = "")
    aov_out_gene[[gene]] <- out[out$var=="gene1",]
  }
  
  aov_out_gene <- as.data.frame(do.call("rbind",aov_out_gene),stringsAsFactors=F)
  aov_out_gene$q <- p.adjust(aov_out_gene$p,method="BH")
  aov_out_gene$var <- NULL
  cfint <- as.data.frame(do.call("rbind",cfint),stringsAsFactors=F)
  rownames(cfint) <- NULL
  coefs <- do.call("rbind",coefs)
  return(list(aov_out_gene=aov_out_gene,cfint=cfint,coefs=coefs))
}

run_all <- function(res,ids,genes_main,fname_out){
  library(WriteXLS)
  out <- make_merged_plot(res,ids,genes_main)
  ord <- out$ord
  p1 <- out$g
  
  q.aovp <- run_test(res,ids)
  dir.create(dirname(fname_out),recursive = T,showWarnings = F)
  WriteXLS(q.aovp,ExcelFileName = gsub(fname_out,pattern="\\.pdf",replacement = ".xlsx"),
           SheetNames = names(q.aovp))
  q.aovp <- q.aovp$aov_out_gene[,c("gene","q")]
  colnames(q.aovp)[colnames(q.aovp)=="q"] <- "q.aovp"
  q.aovp$gene <- NULL
  q.aovp$dummy <- NA
  q.aovp <- q.aovp[order(rownames(q.aovp)),]
  q.aovp$dummy <- NULL
  df <- q.aovp
  df$dummy <- NA
  df <- df[order(apply(df,1,min),decreasing = F),]
  df$gene <- rownames(df)
  df$dummy <- NULL
  df <- merge(df,ord,by="gene")
  df <- rbind(df,c(1,0.99,0))
  df$is_main <- df$gene %in% genes_main
  
  library(ggplot2)
  p2 <- ggplot(df,aes(x=reorder(gene,ord),y=-log10(q.aovp))) + geom_bar(stat="identity") + theme_classic() +
    theme(axis.text.x = element_text(angle = 90, hjust = 1,size=5)) + scale_y_reverse() + 
    theme(axis.text=element_text(size=5),axis.title=element_text(size=5)) +
    geom_hline(yintercept = -log10(0.05),color="gray", linetype="dashed") + 
    facet_wrap(~is_main,scales="free_x",drop=TRUE) + theme(strip.background = element_blank(),
                                                           strip.text.x = element_blank())
  
  library(gridExtra)
  dev.off()
  pdf(fname_out,width=5,height=5)
  print(grid.arrange(p1, p2, ncol=1))
  dev.off()
  
  return(df)
}

fnames <- list.files("~/um/code/data/Figure_2h_S3c/",pattern="csv$",full.names = T)

res <- list()
for (fname in fnames){
  res[[basename(fname)]] <- read.table(fname,sep=",",header=T,row.names = 1)
}

genes_main <- c("MAPK14","RAC1","PTK2","PLCG1","SUMO3","CSNK2A1","GNAQ","control siRNA","GNAQ")

id.UM22 <- "Cell count UM22_72 hrs.csv"
id.MP41 <- "Cell count MP-41_96 hrs.csv"
id.92_1 <- "Cell count 92-1_96 hrs.csv"
ids <- c(id.UM22,id.MP41,id.92_1)

df.cell_count <- run_all(res,ids,genes_main,fname_out=paste0(outdir,"/Figure_2h_S3c.cell_count.pdf"))

id.UM22 <- "CTG for Cell count UM22_72 hrs.csv"
id.MP41 <- "CTG for Cell count MP-41_96 hrs.csv"
id.92_1 <- "CTG for Cell count 92-1_96 hrs.csv"
ids <- c(id.UM22,id.MP41,id.92_1)

df.lumi <- run_all(res,ids,genes_main,fname_out=paste0(outdir,"/Figure_2h_S3c.lumiscence.pdf"))

# [Supplementary Data 3] --------------------------------------
library(readxl)
fnames <- Sys.glob(paste0(outdir,"/*.xlsx"))
tabs <- list()
for (fname in fnames){
  sheets <- excel_sheets(fname)
  tabs.sheet <- list()
  for (sheet in sheets){
    tabs.sheet[[sheet]] <- as.data.frame(read_excel(fname,sheet = sheet),stringsAsFactors=F)
  }
  tabs[[basename(fname)]] <- tabs.sheet
}

tabs.merged <- list()
for (sname in names(tabs)){
  a <- tabs[[sname]]$aov_out_gene
  b <- tabs[[sname]]$cfint[tabs[[sname]]$cfint[,"var"]=="gene1",]
  d <- tabs[[sname]]$coefs[tabs[[sname]]$coefs[,"var"]=="gene1",]
  x <- merge(a,b,by="gene",all.x=T,all.y=T)
  x$var <- NULL
  x$R_sum_Sq <- NULL
  x$R_Mean_Sq <- NULL
  x <- merge(x,d,by="gene",all.x=T,all.y=T)
  x$var <- NULL
  colnames(x)[colnames(x)=="Df"] <- "Degrees of freedom"
  colnames(x)[colnames(x)=="coefficient"] <- "Coefficient"
  colnames(x)[colnames(x)=="gene"] <- "Gene"
  x <- x[,c("Gene","p","q","Coefficient","2.5 %","97.5 %","Degrees of freedom")]
  tabs.merged[[sname]] <- x
}
library(stringr)
names(tabs.merged) <- str_split_fixed(str_split_fixed(names(tabs.merged),"Figure_2h_S3c\\.",2)[,2],"\\.",2)[,1]
names(tabs.merged)[names(tabs.merged)=="cell_count"] <- "siRNA - Cell count"
names(tabs.merged)[names(tabs.merged)=="lumiscence"] <- "siRNA - Luminescence"

fname.cnv_analysis <- "~/um/Figures_and_tables/Supplement/Supplementary_Data_3.cnv_analysis.xlsx"
sheets <- excel_sheets(fname.cnv_analysis)
cnv_analysis <- list()
for (sheet in sheets){
  cnv_analysis[[sheet]] <- as.data.frame(read_excel(fname.cnv_analysis,sheet = sheet),stringsAsFactors=F)
}

amp_fname <- "~/um/Figures_and_tables/Figure_2g/amp_candidates.msigdb.xlsx"
del_fname <- "~/um/Figures_and_tables/Figure_2g/del_candidates.msigdb.xlsx"
cnv_enrichment <- list()
cnv_enrichment[["Gain"]] <- as.data.frame(read_excel(amp_fname),stringsAsFactors=F)
cnv_enrichment[["Loss"]] <- as.data.frame(read_excel(del_fname),stringsAsFactors=F)

tab <- c(cnv_analysis,cnv_enrichment,tabs.merged)
names(tab)

WriteXLS(tab,ExcelFileName = "~/um/Figures_and_tables/Supplement/Supplementary_Data_3.xlsx",
         SheetNames = names(tab))

# [Supplementary Figure 3d] ------------------------------------------
library(readxl)

fnames <- list.files("~/um/code/data/Supplementary_Figure_3d/",pattern="xlsx$",full.names = T)
res <- list()
for (fname in fnames){
  sheets <- excel_sheets(fname)
  sheets <- sheets[grep(sheets,pattern="Final")]
  res[[basename(fname)]] <- read_excel(fname,sheet = sheets)
  res[[basename(fname)]] <- as.data.frame(res[[basename(fname)]],stringsAsFactors=F)
  res[[basename(fname)]] <- res[[basename(fname)]][,c("...1","% Gene...14","% Gene...15")]
  res[[basename(fname)]] <- res[[basename(fname)]][!is.na(res[[basename(fname)]][["% Gene...14"]]),]
  colnames(res[[basename(fname)]]) <- c("gene","rep1","rep2")
}
res <- as.data.frame(do.call("rbind",res),stringsAsFactors=F)
res$gene[res$gene=="TRIM27 UB"] <- "TRIM27"
rownames(res) <- NULL
res <- as.data.frame(rbind(res,c("Control siRNA",100,100)),stringsAsFactors=F)
res$rep1 <- as.numeric(res$rep1)
res$rep2 <- as.numeric(res$rep2)

res <- reshape2::melt(res,id.vars="gene")

dir.create("~/um/Figures_and_tables/Supplement/",recursive = T,showWarnings = F)
ggplot(res,aes(x=reorder(gene,-value),y=value)) + stat_summary(geom = "bar", fun.y = mean, position = "dodge") + 
  stat_summary(geom = "errorbar", fun.data = mean_se, position = "dodge") + theme_classic() +
  geom_point(size=1) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + xlab(NULL) + ylab("Relative gene expression (%)")
ggsave(file="~/um/Figures_and_tables/Supplement/Supplementary_Figure_3d.pdf",width=4,heigh=3)